#!/usr/bin/env python

from setuptools import setup
import os, sys

try:
    import multiprocessing
except ImportError:
    pass

setup_path = os.path.dirname(__file__)
reqs_file = open(os.path.join(setup_path, 'requirements.txt'), 'r')
reqs = reqs_file.readlines()
reqs_file.close()

setup(
    name="troz",
    version="0.3.25",
    description="Python Client for Troz IO",
    author="Guilherme",
    author_email="guilherme.coltro@cazamba.com",
    url="https://bitbucket.org/TrozIO/client-python",
    packages=["troz"],
    install_requires=reqs
)