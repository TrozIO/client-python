from keen.client import KeenClient
from keen.api import KeenApi

__author__ = 'c011r0'

class TrozClient(KeenClient):
    def __init__(self, project_id, write_key=None, read_key=None,
                 persistence_strategy=None, api_class=KeenApi, get_timeout=305, post_timeout=305,
                 master_key=None, base_url="https://api.troz.io/", api_version=2.0):
        super(TrozClient, self).__init__(project_id, write_key=write_key, read_key=read_key,
                                         persistence_strategy=persistence_strategy, api_class=api_class, get_timeout=get_timeout, post_timeout=post_timeout,
                                         master_key=master_key, base_url=base_url)
        self.api = api_class(project_id, write_key=write_key, read_key=read_key,
                             get_timeout=get_timeout, post_timeout=post_timeout,
                             master_key=master_key, base_url=base_url, api_version=api_version)

    '''
        Declaring new operations
    '''
    def select_unique(self, event_collection, target_property, timeframe=None, timezone=None, interval=None,
                     filters=None, group_by=None, max_age=None, latest=None, order_by=None, arrange_by=None):
        params = self.get_params(event_collection=event_collection, timeframe=timeframe, timezone=timezone,
                                 interval=interval, filters=filters, group_by=group_by,
                                 target_property=target_property, max_age=max_age, latest=latest, order_by=order_by, arrange_by=arrange_by)
        return self.api.query("select_unique", params)
    
    def count(self, event_collection, timeframe=None, timezone=None, interval=None,
              filters=None, group_by=None, max_age=None, latest=None, order_by=None, arrange_by=None):
        params = self.get_params(event_collection=event_collection, timeframe=timeframe, timezone=timezone,
                                 interval=interval, filters=filters, group_by=group_by, max_age=max_age, latest=latest, order_by=order_by, arrange_by=arrange_by)
        return self.api.query("count", params)

    def sum(self, event_collection, target_property, timeframe=None, timezone=None, interval=None, filters=None,
            group_by=None, max_age=None, latest=None, order_by=None, arrange_by=None):
        params = self.get_params(event_collection=event_collection, timeframe=timeframe, timezone=timezone,
                                 interval=interval, filters=filters, group_by=group_by,
                                 target_property=target_property, max_age=max_age, latest=latest, order_by=order_by, arrange_by=arrange_by)
        return self.api.query("sum", params)

    def minimum(self, event_collection, target_property, timeframe=None, timezone=None, interval=None,
                filters=None, group_by=None, max_age=None, latest=None, order_by=None, arrange_by=None):
        params = self.get_params(event_collection=event_collection, timeframe=timeframe, timezone=timezone,
                                 interval=interval, filters=filters, group_by=group_by,
                                 target_property=target_property, max_age=max_age, latest=latest, order_by=order_by, arrange_by=arrange_by)
        return self.api.query("minimum", params)

    def maximum(self, event_collection, target_property, timeframe=None, timezone=None, interval=None,
                filters=None, group_by=None, max_age=None, latest=None, order_by=None, arrange_by=None):
        params = self.get_params(event_collection=event_collection, timeframe=timeframe, timezone=timezone,
                                 interval=interval, filters=filters, group_by=group_by,
                                 target_property=target_property, max_age=max_age, latest=latest, order_by=order_by, arrange_by=arrange_by)
        return self.api.query("maximum", params)

    def average(self, event_collection, target_property, timeframe=None, timezone=None, interval=None,
                filters=None, group_by=None, max_age=None, latest=None, order_by=None, arrange_by=None):
        params = self.get_params(event_collection=event_collection, timeframe=timeframe, timezone=timezone,
                                 interval=interval, filters=filters, group_by=group_by,
                                 target_property=target_property, max_age=max_age, latest=latest, order_by=order_by, arrange_by=arrange_by)
        return self.api.query("average", params)

    def count_unique(self, event_collection, target_property, timeframe=None, timezone=None, interval=None,
                     filters=None, group_by=None, max_age=None, latest=None, order_by=None, arrange_by=None):
        params = self.get_params(event_collection=event_collection, timeframe=timeframe, timezone=timezone,
                                 interval=interval, filters=filters, group_by=group_by,
                                 target_property=target_property, max_age=max_age, latest=latest, order_by=order_by, arrange_by=arrange_by)
        return self.api.query("count_unique", params)

    def get_params(self, event_collection=None, timeframe=None, timezone=None, interval=None, filters=None,
                   group_by=None, target_property=None, latest=None, email=None, analyses=None, steps=None,
                   property_names=None, percentile=None, max_age=None, order_by=None, arrange_by=None):
        params = super(TrozClient, self).get_params(event_collection=event_collection, timeframe=timeframe, timezone=timezone, interval=interval, filters=filters,
                   group_by=group_by, target_property=target_property, latest=latest, email=email, analyses=analyses, steps=steps,
                   property_names=property_names, percentile=percentile, max_age=max_age)
        if order_by:
            params["order_by"] = order_by
        if arrange_by:
            params["arrange_by"] = arrange_by
        return params